import React from 'react'
import { PromiseCard } from './PromiseCard'

export const PromiseGrip = () => {

    return (
        <div className="urban-grip">
            <div className="row">
                <div className="col-lg-4 col-md-6 mb-4">
                    <PromiseCard />
                </div>
                <div className="col-lg-4 col-md-6 mb-4">
                    <PromiseCard />
                </div>
                <div className="col-lg-4 col-md-6 mb-4">
                    <PromiseCard />
                </div>
            </div>
        </div>
    )
}

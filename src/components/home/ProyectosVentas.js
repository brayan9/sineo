import React from 'react'
import { CardSell } from './CardSell'
export const ProyectosVentas = () => {
    return (
        <div className="ventas">
            <div className="title">
                <div className="title-left">
                    <h1>Proyectos <b>en venta</b></h1>
                </div>
                <div className="title-right">
                    <h3><b>Todos</b> los proyectos</h3>
                    <span class="material-icons md-black">
                        arrow_right_alt
                    </span>
                </div>
            </div>
            <div className="grid">
                <div className="row">
                    <div className="col-lg-4 col-md-6 mb-4">
                        <CardSell />
                    </div>
                    <div className="col-lg-4 col-md-6 mb-4">
                        <CardSell />
                    </div>
                    <div className="col-lg-4 col-md-6 mb-4">
                        <CardSell />
                    </div>
                    <div className="col-lg-4 col-md-6 mb-4">
                        <CardSell />
                    </div>
                    <div className="col-lg-4 col-md-6 mb-4">
                        <CardSell />
                    </div>
                    <div className="col-lg-4 col-md-6 mb-4">
                        <CardSell />
                    </div>
                </div>
            </div>
        </div>
    )
}

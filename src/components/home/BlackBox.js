import React from 'react'

export const BlackBox = () => {
    return (
        <div className="black-box">
            <h2>Te ayudamos a encontrar tu <b>nuevo siena</b></h2>
            <div className="black-forms">
                <form className="drop-list">
                    <select name="dropdown" className="list">
                        <option disabled selected>Tipo</option>
                    </select>
                </form>
                <form className="drop-list">
                    <select name="dropdown" className="list">
                        <option disabled selected>Comuna</option>
                    </select>
                </form>
                <button className="btn">
                    <b>Buscar</b>
                </button>
            </div>
        </div>
    )
}

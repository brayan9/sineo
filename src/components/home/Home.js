import React from 'react';
import { Advertencia } from './Advertencia';
import { NavMenu } from '../ui/NavMenu';
import { CarrouselLanding } from './CarrouselLanding';
import { BlackBox } from './BlackBox';
import { ProyectosVentas } from './ProyectosVentas';
import { Promocion } from './Promocion';
import { Tendencia } from './Tendencia';
import { RedBox } from './RedBox';
import { Footer } from '../ui/Footer';
export const Home = () => {
    return (
        <>
            <Advertencia />
            <div className="landing">
                <NavMenu />
                <CarrouselLanding />
                <BlackBox />
            </div>
            <div className="container">
                <ProyectosVentas />
                <Promocion />
                <Tendencia />
            </div>
            <div className="foot">
                <RedBox />
                <Footer />
            </div>
        </>
    )
}

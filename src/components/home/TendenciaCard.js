import React from 'react';
import patio from '../../img/patiovs.jpg';
export const TendenciaCard = () => {
    return (
        <div className="tendencia-card">
            <div className="tendencia-imagen">
                <div className="background"></div>
                <img src={patio}/>
                <h2><b>Beneficios de invertir en propiedades inmobiliarias</b></h2>
                <span class="material-icons md-red">
                    arrow_right_alt
                </span>
            </div>
            <div className="tendencia-info">
                <p>14 - 08 - 2019</p>
                <p>Comprar bienes raíces es una de las formas de invertir más rentable. Para 2019 se pronosticó un crecimiento de 10% en las ventas de viviendas en Chile.</p>
            </div>
        </div>
    )
}

import React from 'react';
import patio from '../../img/patio.jpg';
import bed from '../../img/bed.svg';
import plane from '../../img/plan.svg';
import crane from '../../img/crane.svg';
import ubication from '../../img/location.png';

export const CardSell = () => {
    return (
        <div className="sell">
            <div className="sell-galeria">
                <div className="sell-image">
                    <img src={patio} />
                </div>
                <div className="sell-ubicacion">

                </div>
                <div className="sell-arrows">

                </div>
            </div>
            <div className="sell-info">
                <h3><b>Canquén Norte Etapa IV</b></h3>
                <div className="descripcion">
                    <img src={bed} />
                    <p>3 a 4 dormitorios</p>
                </div>
                <div className="descripcion">
                    <img src={plane} />
                    <p>147 mt + terraza</p>
                </div>
                <div className="descripcion">
                    <img src={crane} />
                    <p>Label</p>
                </div>
                <hr />
                <h4>Desde: <b>Valor</b></h4>
            </div>
        </div>
    )
}

import React from 'react'
import { PromiseGrip } from './PromiseGrip'
import { TendenciaCard } from './TendenciaCard'
export const Tendencia = () => {
    return (
        <div className="tendencia">
            <div className="title">
                <div className="title-left">
                    <h1>Tendencias <b>urbanas</b></h1>
                </div>
                <div className="title-right">
                    <h3><b>Todas</b> Las tendencias</h3>
                    <span class="material-icons md-black">
                        arrow_right_alt
                    </span>
                </div>
            </div>
            <div className="tendencia-anuncio row">
                <div className="card-left col-md-6">
                    <TendenciaCard />
                </div>
                <div className="card-right col-md-6">
                    <TendenciaCard />
                </div>
            </div>
            <div className="tendencia-grid">
                <PromiseGrip />
            </div>
        </div>
    )
}

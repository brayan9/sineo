import React from 'react';
import {PromocionCard} from './PromocionCard';

export const Promocion = () => {

    return (
        <div className="promocion">
            <div className="title">
                <div className="title-left">
                    <h1>Unidades <b>en promocion</b></h1>
                </div>
                <div className="title-right">
                    <h3><b>Más</b> promociones</h3>
                    <span class="material-icons md-black">
                        arrow_right_alt
                    </span>
                </div>
            </div>
            <div className="promo-grid row">
                <div className="col-lg-4 col-md-6 mb-4">
                    <PromocionCard />
                </div>
                <div className="col-lg-4 col-md-6 mb-4">
                    <PromocionCard />
                </div>
                <div className="col-lg-4 col-md-6 mb-4">
                    <PromocionCard />
                </div>
                
            </div>
        </div>
    )
}

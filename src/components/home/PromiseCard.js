import React from 'react'
import balcon from '../../img/balcon.jpg'
export const PromiseCard = () => {

    return (
        <div className="urban-card">
            <div className="urban-card-img">
                <img src={balcon} />
                <span class="material-icons md-red">
                    arrow_right_alt
                </span>
            </div>
            <div className="urban-card-info">
                <h2>Loren <b>Ipsum</b></h2>
                <p>Lorem ipsum dolor sit amet consectetur, adipiscing elit nam venenatis imperdiet elementum, dictumst sic dolore.</p>
            </div>
        </div>
    )
}

import React from 'react'
import bed from '../../img/bed.svg';
import patio from '../../img/patio.jpg';
import plane from '../../img/plan.svg';
import piso from '../../img/piso.svg';
import ubication from '../../img/location.png';

export const PromocionCard = () => {
    return (
        <div className="promo">
            <div className="promo-galeria">
                <div className="promo-image">
                    <img src={patio}/>
                </div>
                <div className="promo-ubicacion">

                </div>
                <div className="promo-arrows">
                    
                </div>
            </div>
            <div className="promo-info">
                <h2><b>Nombre Unidad</b></h2>
                <h3><b>Nombre Proyecto</b></h3>
                <div className="descripcion">
                    <img src={bed}/>
                    <p>3 a 4 dormitorios</p>
                </div>
                <div className="descripcion">
                    <img src={plane}/>
                    <p>147 mt + terraza</p>
                </div>
                <div className="descripcion">
                    <img src={piso}/>
                    <p>Piso 4</p>
                </div>
                <hr/>
                <p>Precio lista: Valor</p>
                <h2>Precio web: <b>UF 13.342</b></h2>
                <h3 style={{color:"red"}}>Reserva online: <b>$500.000</b></h3>
                <p><b>Incluye:</b></p>
                <ul>
                    <li>Gift Card de $500.000</li>
                    <li>Focos y rieles de cortinas instalados</li>
                </ul>
            </div>
        </div>
    )
}

import React from 'react'

export const RedBox = () => {
    return (
        <div className="red-box">
            <h3>Mantente al día <b>Sobre nuestros proyectos y ofertas.</b></h3>
            <div className="red-forms">
                <form className="drop-list">
                    <input type="text" id="name" name="name" placeholder="Name" className="list"/>
                </form>
                <form className="drop-list">
                    <input type="text" id="name" name="name" placeholder="Email" className="list"/>
                </form>
                <button className="btn">
                    <b>Suscribirse</b>
                </button>
            </div>
        </div>
    )
}

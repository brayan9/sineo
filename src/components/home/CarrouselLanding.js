import React from 'react'
import casa from '../../img/casa.jpg'
export const CarrouselLanding = () => {
    return (
        <div className="carrousel">
            <div className="cuadro"></div>
            <img src={casa} />
            <div className="carrousel-info">
                <h3>Vive en lo mejor de chicureo</h3>
                <h1><b>Canquén Norte<br />Etapa IV</b></h1>
                <span class="material-icons md-white">
                    arrow_right_alt
                </span>
            </div>
        </div>
    )
}

import React, { useState } from 'react'
import info from '../../img/icono-info.svg'

export const Advertencia = () => {
    const [mostrar, setMostrar] = useState(true);
    const disable = () => {
        setMostrar(false);
    }
    return (
        <>
            {
                mostrar ?
                    <div className="info">
                        <div className="container">
                            <div className="row justify-content-between">
                                <div className="info-text d-flex col-auto me-auto">
                                    <img src={info} className="info-text-info" />
                                    <p className="info-text-content"><b>Horarios Salas de Venta:</b> Lunes a domingo de 10.00 a 13.00 y de 14.00 a 17.00 hrs.</p>
                                </div>
                                
                                <span
                                    className="material-icons md-light close col text-end"
                                    onClick={disable}
                                >
                                    close
                                </span>
                            </div>

                        </div>
                    </div>

                    : (null)
            }

        </>
    )
}

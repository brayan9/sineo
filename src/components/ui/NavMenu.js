import React from 'react'
import siena from '../../img/siena.svg'
import { Link, NavLink } from 'react-router-dom'

export const NavMenu = () => {

    return (
        <nav className="navbar navbar-expand-md">
            <div className="container">
                <Link
                    className="navbar-brand"
                    to="/"
                >
                    <img src={siena} />
                </Link>

                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div className="navbar-collapse collapse justify-content-end" id="navbarTogglerDemo02">
                    <div className="navbar-nav text-end">

                        <NavLink
                            activeClassName="active"
                            className="nav-item nav-link"
                            exact
                            to="/home"
                        >
                            <b>Proyectos</b>
                        </NavLink>


                        <NavLink
                            activeClassName="active"
                            className="nav-item nav-link"
                            exact
                            to="/single"
                        >
                            <b>Alianzas</b>
                        </NavLink>

                        <NavLink
                            activeClassName="active"
                            className="nav-item nav-link"
                            exact
                            to="/home"
                        >
                            <b>Promociones</b>
                        </NavLink>
                        <NavLink
                            activeClassName="active"
                            className="nav-item nav-link"
                            exact
                            to="/home"
                        >
                            <b>Financiamiento</b>
                        </NavLink>
                        <NavLink
                            activeClassName="active"
                            className="nav-item nav-link"
                            exact
                            to="/home"
                        >
                            <b>Atención al Cliente</b>
                        </NavLink>
                        <NavLink
                            activeClassName="active"
                            className="nav-item nav-link"
                            exact
                            to="/home"
                        >
                            <b>Inversionistas</b>
                        </NavLink>
                    </div>
                </div>
            </div>

        </nav>

    )
}

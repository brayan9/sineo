import React from 'react'
import face from '../../img/facebook.png'
import insta from '../../img/instagram.png'
import youtube from '../../img/youtube.png'
import siena from '../../img/siena.svg'

export const Footer = () => {
    return (
        <div className="group-footer">
            <div className="container">
                <img src={siena} />
                <div className="footer">
                    <div className="footer-content">
                        <p>Los Militares 6191, Piso 11<br />Las Condes, Santiago de Chile</p>
                        <p>Teléfono: +56 2 2571 7500</p>
                        <p>Fax: +56 2 2571 7555</p>
                        <p>Post-Venta: +56 2 25717500 (anexo 3)</p>
                    </div>
                    <div className="footer-content">
                        <a href="#">Proyectos</a>
                        <br />
                        <a href="#">Promociones</a>
                        <br />
                        <a href="#">Tendencias Urbanas</a>
                        <br />
                        <a href="#">Financiamiento</a>
                        <br />
                        <a href="#">Somos Siena</a>
                        <br />
                        <a href="#">Inversionistas</a>
                        <br />
                        <a href="#">Atención a clientes</a>

                    </div>
                    <div className="footer-content">
                        <div className="footer-redes">
                            <img src={face} href="#" />
                            <a href="#">/SienaInmobiliaria</a>
                        </div>
                        <div className="footer-redes">
                            <img src={insta} href="#" />
                            <a href="#">@sienainmobiliaria</a>
                        </div>
                        <div className="footer-redes">
                            <img src={youtube} href="#" />
                            <a href="#">Inmobiliaria Siena</a>
                        </div>
                    </div>
                    <div className="footer-content footer-chiquito">
                        <a href="#">Términos y Condiciones</a>
                        <br />
                        <a href="#">Acceso a intranet Siena</a>
                        <br />
                        <a href="#">Acceso a Empresas</a>
                    </div>
                </div>
            </div>
        </div>
    )
}

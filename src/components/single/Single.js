import React from 'react'
import { CarrouselLanding } from '../home/CarrouselLanding'
import { Footer } from '../ui/Footer'
import { NavMenu } from '../ui/NavMenu'
import { RedBox } from '../home/RedBox'

export const Single = () => {
    return (
        <div>
            <div className="landing">
                <NavMenu />
                <CarrouselLanding />
            </div>

            <div className="foot">
                <RedBox />
                <Footer />
            </div>
        </div>
    )
}

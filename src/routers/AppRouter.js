import React from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
  } from 'react-router-dom';
  
import { Home } from '../components/home/Home';
import { Single } from '../components/single/Single';

export const AppRouter = () => {
    return (
        <Router>
            <div>
                <Switch> 
                    <Route exact path="/home" component={ Home } />
                    <Route exact path="/single" component={ Single } />
                    <Redirect to="/home" />
                </Switch>
            </div>
        </Router>
    )
}

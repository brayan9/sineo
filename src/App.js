import React from 'react';
import { AppRouter } from './routers/AppRouter';

export const App = () => {

  return (
    <div className="margin-all">
      <AppRouter />
    </div>
  )
}
